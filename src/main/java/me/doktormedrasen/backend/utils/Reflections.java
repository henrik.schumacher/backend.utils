/*
 * Decompiled with CFR 0.139.
 */
package me.doktormedrasen.backend.utils;

import java.lang.reflect.Field;

public class Reflections {
    public static void setField(Object edit, String fieldname, Object value) {
        try {
            Field field = edit.getClass().getDeclaredField(fieldname);
            field.setAccessible(true);
            field.set(edit, value);
        }
        catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public static <T> T getField(Object object, String fieldname) {
        try {
            Field field = object.getClass().getDeclaredField(fieldname);
            field.setAccessible(true);
            return (T)field.get(object);
        }
        catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }
}

