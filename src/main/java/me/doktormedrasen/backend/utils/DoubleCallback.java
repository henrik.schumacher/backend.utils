/*
 * Decompiled with CFR 0.139.
 */
package me.doktormedrasen.backend.utils;

public interface DoubleCallback<T1, T2> {
    public void done(T1 var1, T2 var2);
}

