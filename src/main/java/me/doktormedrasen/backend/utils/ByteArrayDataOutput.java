/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.google.common.io.ByteArrayDataOutput
 *  com.google.common.io.ByteStreams
 */
package me.doktormedrasen.backend.utils;

import com.google.common.io.ByteStreams;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class ByteArrayDataOutput {
    private com.google.common.io.ByteArrayDataOutput output = ByteStreams.newDataOutput();

    public byte[] toByteArray() {
        return this.output.toByteArray();
    }

    public ByteArrayDataOutput writeByte(byte b) {
        this.output.writeByte((int)b);
        return this;
    }

    public ByteArrayDataOutput writeByteArray(byte[] bytes) {
        this.output.writeInt(bytes.length);
        for (byte b : bytes) {
            this.output.writeByte((int)b);
        }
        return this;
    }

    public ByteArrayDataOutput writeBoolean(boolean b) {
        this.output.writeBoolean(b);
        return this;
    }

    public ByteArrayDataOutput writeChar(char c) {
        this.output.writeChar((int)c);
        return this;
    }

    public ByteArrayDataOutput writeDouble(double d) {
        this.output.writeDouble(d);
        return this;
    }

    public ByteArrayDataOutput writeFloat(float f) {
        this.output.writeFloat(f);
        return this;
    }

    public ByteArrayDataOutput writeInt(int i) {
        this.output.writeInt(i);
        return this;
    }

    public ByteArrayDataOutput writeLong(long l) {
        this.output.writeLong(l);
        return this;
    }

    public ByteArrayDataOutput writeString(String s) {
        byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
        this.output.writeInt(bytes.length);
        for (byte b : bytes) {
            this.output.writeByte((int)b);
        }
        return this;
    }

    public ByteArrayDataOutput writeUUID(UUID uuid) {
        this.output.writeLong(uuid.getMostSignificantBits());
        this.output.writeLong(uuid.getLeastSignificantBits());
        return this;
    }
}

