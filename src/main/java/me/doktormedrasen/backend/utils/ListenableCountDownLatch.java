/*
 * Decompiled with CFR 0.139.
 */
package me.doktormedrasen.backend.utils;

public class ListenableCountDownLatch {
    private int count;
    private Runnable runnable;

    public ListenableCountDownLatch(int count) {
        this.count = count;
    }

    public synchronized void countdown() {
        --this.count;
        if (this.count == 0 && this.runnable != null) {
            this.runnable.run();
        }
    }

    public synchronized void setListener(Runnable runnable) {
        this.runnable = runnable;
    }
}

