/*
 * Decompiled with CFR 0.139.
 */
package me.doktormedrasen.backend.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class ThreadSafeArrayList<T>
implements List<T> {
    private List<T> list;
    private Lock readLock;
    private Lock writeLock;

    private ThreadSafeArrayList(ArrayList<T> list) {
        this.list = list;
        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        this.readLock = readWriteLock.readLock();
        this.writeLock = readWriteLock.writeLock();
    }

    public ThreadSafeArrayList() {
        this(new ArrayList());
    }

    public ThreadSafeArrayList(int initialCapacity) {
        this(new ArrayList(initialCapacity));
    }

    public ThreadSafeArrayList(Collection<T> from) {
        this(new ArrayList<T>(from));
    }

    @Override
    public int size() {
        this.readLock.lock();
        try {
            int n = this.list.size();
            return n;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public boolean isEmpty() {
        this.readLock.lock();
        try {
            boolean bl = this.list.isEmpty();
            return bl;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public boolean contains(Object o) {
        this.readLock.lock();
        try {
            boolean bl = this.list.contains(o);
            return bl;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public Iterator<T> iterator() {
        this.readLock.lock();
        try {
            Iterator<T> iterator = this.list.iterator();
            return iterator;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public Object[] toArray() {
        this.readLock.lock();
        try {
            Object[] arrobject = this.list.toArray();
            return arrobject;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        this.readLock.lock();
        try {
            T1[] arrT1 = this.list.toArray(a);
            return arrT1;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public boolean add(T t) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.add(t);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public boolean remove(Object o) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.remove(o);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public boolean containsAll(Collection c) {
        this.readLock.lock();
        try {
            boolean bl = this.list.containsAll(c);
            return bl;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.addAll(c);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.addAll(index, c);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.removeAll(c);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public boolean retainAll(Collection c) {
        this.writeLock.lock();
        try {
            boolean bl = this.list.retainAll(c);
            return bl;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public void clear() {
        this.writeLock.lock();
        try {
            this.list.clear();
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public T get(int index) {
        this.readLock.lock();
        try {
            T t = this.list.get(index);
            return t;
        }
        finally {
            this.readLock.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public T set(int index, T element) {
        this.writeLock.lock();
        try {
            T t = this.list.set(index, element);
            return t;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public void add(int index, T element) {
        this.writeLock.lock();
        try {
            this.list.add(index, element);
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public T remove(int index) {
        this.writeLock.lock();
        try {
            T t = this.list.remove(index);
            return t;
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public int indexOf(Object o) {
        this.readLock.lock();
        try {
            int n = this.list.indexOf(o);
            return n;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public int lastIndexOf(Object o) {
        this.readLock.lock();
        try {
            int n = this.list.lastIndexOf(o);
            return n;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public ListIterator<T> listIterator() {
        this.readLock.lock();
        try {
            ListIterator<T> listIterator = this.list.listIterator();
            return listIterator;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        this.readLock.lock();
        try {
            ListIterator<T> listIterator = this.list.listIterator(index);
            return listIterator;
        }
        finally {
            this.readLock.unlock();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        this.readLock.lock();
        try {
            List<T> list = this.list.subList(fromIndex, toIndex);
            return list;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public void replaceAll(UnaryOperator<T> operator) {
        this.writeLock.lock();
        try {
            this.list.replaceAll(operator);
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public void sort(Comparator<? super T> c) {
        this.writeLock.lock();
        try {
            this.list.sort(c);
        }
        finally {
            this.writeLock.unlock();
        }
    }

    @Override
    public Spliterator<T> spliterator() {
        this.readLock.lock();
        try {
            Spliterator<T> spliterator = this.list.spliterator();
            return spliterator;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        this.readLock.lock();
        try {
            boolean bl = this.list.removeIf(filter);
            return bl;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public Stream<T> stream() {
        this.readLock.lock();
        try {
            Stream stream = this.list.stream();
            return stream;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public Stream<T> parallelStream() {
        this.readLock.lock();
        try {
            Stream stream = this.list.parallelStream();
            return stream;
        }
        finally {
            this.readLock.unlock();
        }
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        this.readLock.lock();
        try {
            this.list.forEach(action);
        }
        finally {
            this.readLock.unlock();
        }
    }
}

