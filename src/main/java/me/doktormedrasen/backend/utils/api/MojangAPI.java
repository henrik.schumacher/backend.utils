/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.JsonArray
 *  com.google.gson.JsonElement
 *  com.google.gson.JsonObject
 *  com.google.gson.JsonParser
 */
package me.doktormedrasen.backend.utils.api;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MojangAPI {
    public static MojangStatus getStatus(MojangModule module) {
        try {
            URL url = new URL("https://status.mojang.com/check");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            JsonArray array = new JsonParser().parse(reader.readLine()).getAsJsonArray();
            for (JsonElement element : array) {
                JsonObject object = element.getAsJsonObject();
                if (object.get(module.toString()) == null) continue;
                return MojangStatus.valueOf(object.get(module.toString()).getAsString().toUpperCase());
            }
            return MojangStatus.RED;
        }
        catch (Exception e) {
            return MojangStatus.RED;
        }
    }

    public static UUID getUUID(String username) {
        try {
            URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + username);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            JsonObject object = new JsonParser().parse(reader.readLine()).getAsJsonObject();
            return MojangAPI.getUniqueID(object.get("id").getAsString());
        }
        catch (Exception e) {
            return null;
        }
    }

    public static UUID getUUIDAt(String username, long timemillis) {
        try {
            URL url = new URL("https://api.mojang.com/users/profiles/minecraft/" + username + "?at=" + timemillis);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            JsonObject object = new JsonParser().parse(reader.readLine()).getAsJsonObject();
            return MojangAPI.getUniqueID(object.get("id").getAsString());
        }
        catch (Exception e) {
            return null;
        }
    }

    public static UUID getUniqueID(String uuid) {
        return UUID.fromString(uuid.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

    public static String getName(String uuid) {
        List<MojangNameChange> list = MojangAPI.getNameHistory(uuid);
        if (list == null) {
            return null;
        }
        return list.get(list.size() - 1).getName();
    }

    public static List<MojangNameChange> getNameHistory(String uuid) {
        try {
            URL url = new URL("https://api.mojang.com/user/profiles/" + uuid.replace("-", "") + "/names");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            MojangNameChange[] array = (MojangNameChange[])new Gson().fromJson(reader.readLine(), MojangNameChange[].class);
            return Arrays.asList(array);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HashMap<String, String> getProfile(String uuid) {
        try {
            URL url = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.replace("-", "") + "?unsigned=false");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            JsonObject object = (JsonObject)new JsonParser().parse(reader.readLine());
            HashMap<String, String> map = new HashMap<String, String>();
            JsonObject properties = (JsonObject)object.getAsJsonArray("properties").get(0);
            map.put("id", object.get("id").getAsString());
            map.put("name", object.get("name").getAsString());
            map.put("value", properties.get("value").getAsString());
            map.put("signature", properties.get("signature").getAsString());
            return map;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static class MojangNameChange {
        private String name;
        private long changedToAt;

        public MojangNameChange(String name, long changedToAt) {
            this.name = name;
            this.changedToAt = changedToAt;
        }

        public String getName() {
            return this.name;
        }

        public long getChangedToAt() {
            return this.changedToAt;
        }
    }

    public static enum MojangStatus {
        GREEN,
        YELLOW,
        RED;
        
    }

    public static enum MojangModule {
        MINECRAFT("minecraft.net"),
        SESSION("session.minecraft.net"),
        ACCOUNT("account.mojang.com"),
        AUTH("auth.mojang.com"),
        SKINS("skins.minecraft.net"),
        AUTHSERVER("authserver.mojang.com"),
        SESSIONSERVER("sessionserver.mojang.com"),
        API("api.mojang.com"),
        TEXTURES("textures.minecraft.net"),
        MOJANG("mojang.com");
        
        private String name;

        private MojangModule(String name) {
            this.name = name;
        }

        public String toString() {
            return this.name;
        }
    }

}

