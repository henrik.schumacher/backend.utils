/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.google.common.io.ByteArrayDataInput
 *  com.google.common.io.ByteStreams
 */
package me.doktormedrasen.backend.utils;

import com.google.common.io.ByteStreams;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class ByteArrayDataInput {
    private com.google.common.io.ByteArrayDataInput input;

    public ByteArrayDataInput(byte[] bytes) {
        this.input = ByteStreams.newDataInput((byte[])bytes);
    }

    public byte readByte() {
        return this.input.readByte();
    }

    public byte[] readByteArray() {
        byte[] bytes = new byte[this.input.readInt()];
        for (int i = 0; i < bytes.length; ++i) {
            bytes[i] = this.input.readByte();
        }
        return bytes;
    }

    public boolean readBoolean() {
        return this.input.readBoolean();
    }

    public char readChar() {
        return this.input.readChar();
    }

    public double readDouble() {
        return this.input.readDouble();
    }

    public float readFloat() {
        return this.input.readFloat();
    }

    public int readInt() {
        return this.input.readInt();
    }

    public long readLong() {
        return this.input.readLong();
    }

    public String readString() {
        byte[] bytes = new byte[this.input.readInt()];
        for (int i = 0; i < bytes.length; ++i) {
            bytes[i] = this.input.readByte();
        }
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public UUID readUUID() {
        return new UUID(this.input.readLong(), this.input.readLong());
    }
}

