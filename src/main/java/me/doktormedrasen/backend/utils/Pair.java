/*
 * Decompiled with CFR 0.139.
 */
package me.doktormedrasen.backend.utils;

public class Pair<A, B> {
    private A a;
    private B b;

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public A getA() {
        return this.a;
    }

    public B getB() {
        return this.b;
    }

    public boolean equals(Pair pair) {
        return this.getA().equals(pair.getA()) && this.getB().equals(pair.getB());
    }
}

